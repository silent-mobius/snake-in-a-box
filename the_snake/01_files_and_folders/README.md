# Snake in a box



.footer: created By Alex M. Schapelle, VaioLabs.IO

---

# Working with files and folders with Python

- create `python scripts` that will do these next tasks:
  - check list of files in current folder with python
  - open file named `poem.txt` print the content of it.
  - open file named `poem.txt`  print only unique word in it.
  - open file named `poem.txt`  get a phrase that is used most commonly and write it down to file named `hellsing.out`
  - create file named `alucard.thepesh` and write in it name first line of `poem.txt`
  - create folder name `drakul`
  - mv file `alucard.thepesh` into `drakul` folder
  - rename folder `drakul` to `dracula`
  - try to delete folder `dracula`
  - in case of error try to fix it (delete by force) with `exception` handling
  - 