The Bird of Hermes

The Bird of Hermes is my name
Eat my wings to keep me tame

From the bodies blood is shed
Soon the ghouls become undead
The Bird of Hermes comes soon
Not morning, evening or afternoon
The Bird of Hermes feeds on fright
Soon blood shall stain the night
The Bird of Hermes will devour
His enemies at midnight hour 

The Bird of Hermes is my name
Eat my wings to keep me tame

The night shall soon be stained with red
You and your army lay bleeding and dead
Your bodies shall rise soon
And join The Bird’s platoon
You shall learn to live in night
You ghouls are made to fight
The stench of blood is ever sour
The Bird’s enemies shall cower 

The Bird of Hermes is my name
Eat my wings to keep me tame