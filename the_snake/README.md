# The Snake and what he can do in the box
## Python and its modules

- python
    - interactive.
    - ipython - way more interactive.
        - ipython skills
            - ! bang
            - % count
            - %% commands
    
    - snek skills
        - builtin functions
        - variables
        - basic math
          - sequences
        - comments
        - print
        - range
        - execution control
          - if/elif/else
        - loops
          - for
          - while
        - exceptions
        - OOP
        - imports
          - libraries


      - what snek can do ?
        - regex
        - os
        - os.path
        - sys
        - subprocess
        - argparse
        - click
        - fire