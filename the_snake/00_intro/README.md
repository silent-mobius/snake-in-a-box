# Snake in a box



.footer: created By Alex M. Schapelle, VaioLabs.IO

---

# About The Course Itself ?

We'll learn several topics mainly focused on:

- Who needs it ? every one wants to practice python STDLIB
<!-- - How python programming works is something you should know before this  -->
- Do's and don'ts of shell programming.
- Basic concepts of Shell programming.
- Looping and Branching.
- Structures, functions and libraries.
- Automation with shell programming.


### Who Is This course for ?

- Junior sysadmins who wants to practice python3.
- Python developers who wish to learn use different python3 libraries.
- DevOps engineers who would like to practice python3.

---

# Course Topics

- Working with files and folders
- Working with permissions
- Managing users
- Process management
- Scheduling with python
- Package management 
- Network programming and management
- Storage management with python
- Services


---
# About Me
<img src="../99_misc/.img/me.jpg" alt="drawing" style="float:right;width:180px;">

- over 12 years of IT industry Experience.
- fell in love with AS-400 unix system at IDF.
- 5 times tried to finish degree in computer science field
    - between each semester, I tried to take IT course at various places.
        - yes, one of them was A+.
        - yes, one of them was Cisco.
        - yes, one of them was RedHat course.
        - yes, one of them was LPIC1 and Shell scripting.
        - no, others i learned alone.
        - no, not maintaining debian packages any more.
---

# About Me (cont.)
- over 7 years of sysadmin:
    - shell scripting fanatic
    - python developer
    - js admirer
    - golang fallen
    - rust fan
- 5 years of working with devops
    - git supporter
    - vagrant enthusiast
    - ansible consultant
    - container believer
    - k8s user

you can find me on linked-in: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)

