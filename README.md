# Snake In A Box

This is a story of magic snake in a box.
- [The Snake](./the_snake)
- [The Box](./the_box)
- [The Right Mixup](./the_right_mixup)
